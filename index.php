<?php

	include __DIR__ . "/include.php";

	$sistemaDao = new SistemaDAO();

?>

<!DOCTYPE html>
<html>
<head>
	<title>Página Inicial - Sistemas - IFPI Campus Parnaíba</title>


	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#349650">
	<meta charset="utf-8">

	<!-- FontAwesome CSS -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

	<!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

  <link rel="stylesheet" type="text/css" href="./style.css">


</head>
<body>

	<a id="botao-login" class="area-restrita" href="./login">
		<i id="cadeado" class="fas fa-lock"></i> ÁREA RESTRITA
	</a>

	<header>
		<img src="./img/ifpi.svg" class="ifpi-logo">
		<h2 class="subtitulo-site">Sistemas acadêmicos e logísticos</h2>
	</header>

	<form method="get" class="formulario-busca">

		<div class="form-group">
		    <input type="texte" class="form-control" id="busca" name="busca" placeholder="Pesquise por algum sistema." <?php if(!empty($_GET["busca"])) echo "value=\"" . $_GET["busca"] . "\""; ?> >
		</div>
		<button type="submit" class="btn btn-secondary"><i class="fas fa-search"></i></button>

	</form>

	<?php if(!empty($_GET["busca"])): ?>

		<div class="alert alert-light alerta-busca">
			<?php echo $sistemaDao->quantidadeSistemasBusca($_GET["busca"]); ?> resultado(s) encontrados.
			<a href="?" class="btn btn-link" >Voltar ao início</a>
		</div>

		<div class="container">
			<div class="row">

				<?php foreach ($sistemaDao->buscar($_GET["busca"]) as $sistema): ?>

					<div class="col-sm">

						<div class="sistema border rounded">

							<div class="sistema-content">
								<img class="imagem-sistema border rounded" src="./img/sistemas/<?php echo $sistema['imagem']; ?>">
								<div class="texto-sistema">
									<h1 class="nome-sistema"><?php echo $sistema["nome"]; ?></h1>
									<h2 class="descricao-sistema"><?php echo $sistema["descricao"]; ?></h2>
								</div>
							</div>

							<div class="botoes">
								<a href="<?php echo $sistema['endereco']; ?>" class="btn btn-success botao-sistema-home">Acessar</a>
							</div>

						</div>

					</div>

				<?php endforeach; ?>

			</div>
		</div>

	<?php endif; ?>

	<div class="container">
	  <div class="row">

	  	<?php if(empty($_GET["busca"])): ?>
	  		<?php foreach ($sistemaDao->consultarTodos() as $sistema): ?>

			    <div class="col-sm">

			    	<div class="sistema border rounded">

							<div class="sistema-content">
								<img class="imagem-sistema border rounded" src="./img/sistemas/<?php echo $sistema['imagem']; ?>">
								<div class="texto-sistema">
									<h1 class="nome-sistema"><?php echo $sistema["nome"]; ?></h1>
									<h2 class="descricao-sistema"><?php echo $sistema["descricao"]; ?></h2>
								</div>
							</div>

							<div class="botoes">
								<a href="<?php echo $sistema['endereco']; ?>" class="btn btn-success botao-sistema-home">Acessar</a>
							</div>

			    	</div>

			    </div>

	    	<?php endforeach; ?>
		<?php endif; ?>

	  </div>
	</div>

		<script type="text/javascript">

			document.getElementById("botao-login").onmouseover = function() {

				document.getElementById("cadeado").classList.remove("fa-lock");
				document.getElementById("cadeado").classList.toggle("fa-unlock");

			}

			document.getElementById("botao-login").onmouseout = function() {

				document.getElementById("cadeado").classList.remove("fa-unlock");
				document.getElementById("cadeado").classList.toggle("fa-lock");

			}

		</script>

		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

</body>
</html>
