<?php

	class AdminDAO {


		public function existeUsuario($user){

	      $conexao = (new Conexao())->getConexao();
	      $sql = "SELECT * FROM admin WHERE user=:user";

	      $statement = $conexao->prepare($sql);
	      $statement->execute(array(
	        ":user" => $user
	      ));

	      $lista = $statement->fetchAll();

	      return sizeof($lista) > 0;

	    }

			public function login($user, $senha) {

				$conexao = (new Conexao())->getConexao();
	      $sql = "SELECT * FROM admin WHERE user=:user";

	      $statement = $conexao->prepare($sql);
	      $statement->execute(array(
	        ":user" => $user
	      ));

	      $lista = $statement->fetchAll();

	      return password_verify($senha, $lista[0]["senha"]);

			}

	    public function cadastrar($nome, $user, $senha) {

	      try {

	        $conexao = (new Conexao())->getConexao();
	        $sql = "INSERT INTO admin VALUES (DEFAULT,:nome,:user,:pass)";
	        $cadastro = $conexao->prepare($sql);

	        $cadastro->execute(array(
	          ":nome" => $nome,
	          ":user" => $user,
	          ":pass" => password_hash($senha, PASSWORD_BCRYPT)
	        ));

	      }
	      catch(PDOException $e) {
	        throw $e;
	      }

	    }

	    public function consultarTodos() {

	      $conexao = (new Conexao())->getConexao();
	      $sql = "SELECT * FROM admin";
	      $lista = $conexao->query($sql)->fetchAll();

	      return $lista;

	    }

	    public function deletar($user) {

	      try {

	        $conexao = (new Conexao())->getConexao();
	        $sql = "DELETE FROM admin WHERE user=:usuario";
	        $cadastro = $conexao->prepare($sql);

	        $cadastro->execute(array(
	          ":usuario" => $user
	        ));

	      }
	      catch(PDOException $e) {
	        throw $e;
	      }

	    }


	}
