<?php

	class SistemaDAO {

		public function quantidadeSistemasBusca($nome){

			$conexao = (new Conexao())->getConexao();
			$sql = "SELECT count(id) FROM sistemas WHERE nome LIKE :nome";

			$statement = $conexao->prepare($sql);
			$statement->execute(array(
				":nome" => "%" . $nome . "%"
			));

			$resultado = $statement->fetchAll();

			return $resultado[0][0];

		}

		public function buscar($nome){

			$conexao = (new Conexao())->getConexao();
			$sql = "SELECT * FROM sistemas WHERE nome LIKE :nome";

			$statement = $conexao->prepare($sql);
			$statement->execute(array(
				":nome" => "%" . $nome . "%"
			));

			$resultado = $statement->fetchAll();

			return $resultado;

		}

		public function consultarTodos() {

			$conexao = (new Conexao())->getConexao();
			$sql = "SELECT * FROM sistemas";

			$statement = $conexao->query($sql);
			$resultado = $statement->fetchAll();

			return $resultado;

		}

		public function excluir($id) {

			$conexao = (new Conexao())->getConexao();
			$sql = "DELETE FROM sistemas WHERE id=:id";

			$statement = $conexao->prepare($sql);
			$statement->execute(array(
				":id" => $id
			));

		}

		public function cadastrar($nome, $endereco, $descricao, $imagem){

			$conexao = (new Conexao())->getConexao();
			$sql = "SELECT MAX(id) FROM sistemas";
			$consulta = $conexao->query($sql)->fetchAll();
			$banco_id = $consulta[0][0] + 1;

			if($imagem === NULL){
				$nome_arquivo = "default.jpg";
			}
			else {
				$extensao = explode(".", $imagem["name"]);
				$extensao = end($extensao);

				$nome_arquivo = "sistema" . $banco_id . "." . $extensao;
				$caminho = __DIR__ . "/../../img/sistemas/" . $nome_arquivo;
				move_uploaded_file($imagem["tmp_name"], $caminho);
			}

			$sql = "INSERT INTO sistemas VALUES (:id, :nome, :descricao, :endereco, :imagem)";
			$statement = $conexao->prepare($sql);
			$statement->execute(array(
				":id" => $banco_id,
				":nome" => $nome,
				":descricao" => $descricao,
				":endereco" => $endereco,
				":imagem" => $nome_arquivo
			));

		}


	}
