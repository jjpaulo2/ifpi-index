<?php

  include __DIR__ . "/../include.php";

  session_start();
  if(empty($_SESSION["usuario"])) {
    header("Location: ../login/?erro=3");
  }

  $sistemaDao = new SistemaDAO();

  if(!empty($_POST["nome"])){

    try {

      if(!empty($_FILES["foto"]["name"])){
        $sistemaDao->cadastrar($_POST["nome"], $_POST["endereco"], $_POST["descricao"], $_FILES["foto"]);
      }
      else {
        $sistemaDao->cadastrar($_POST["nome"], $_POST["endereco"], $_POST["descricao"], NULL);
      }
      header("Location: ./?sucesso=2");
    }
    catch(PDOException $e) {
      header("Location: ?erro=1");
    }

  }

?>

<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Painel de administrador - Sistemas - IFPI Campus Parnaíba</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#349650">

  	<!-- FontAwesome CSS -->
  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="../style.css">

  </head>
  <body>

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: #349650">
      <a class="navbar-brand" href="#">PAINEL ADMINISTRATIVO</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item">
            <a class="nav-link" href=".">Sistemas <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown active">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Novo
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="#">Sistema</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Usuário</a>
              <a class="dropdown-item" href="#">Senha</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./logout.php">Sair</a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0" action=".">
          <input class="form-control mr-sm-2" type="search" name="busca" placeholder="Buscar sistema"  <?php if(!empty($_GET["busca"])) echo "value=\"" . $_GET["busca"] . "\""; ?>>
          <button class="btn btn-outline-light" type="submit"><i class="fas fa-search"></i></button>
        </form>
      </div>
    </nav>

    <br><br><br>

    <div class="container">

      <form class="bloco-pagina border rounded" method="post" enctype='multipart/form-data'>

        <div class="row">
          <div class="col-sm">

            <div class="form-group">
              <label for="nome">Nome do sistema</label>
              <input type="text" class="form-control" id="nome" name="nome" placeholder="">
            </div>

          </div>
          <div class="col-sm">

            <div class="form-group">
              <label for="endereco">Endereço do sistema</label>
              <input type="text" class="form-control" id="endereco" name="endereco" value="https://">
            </div>

          </div>
        </div>

        <div class="form-group">
          <label for="descricao">Descrição</label>
          <textarea class="form-control" name="descricao" id="descricao" rows="2"></textarea>
        </div>


        <div class="form-group">
          <label for="foto">Imagem do sistema</label>
          <div class="row">
            <div class="col-sm-3">

              <img src="../img/sistemas/default.jpg" id="visualizar-imagem" class="imagem-upload border rounded" alt="">
              <br><br>

            </div>
            <div class="col-sm">

              <input type="file" class="form-control-file input-foto" name="foto" id="foto">

            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-sm"></div>
          <div class="col-sm-3">
            <button type="submit" class="btn btn-success btn-block">Cadastrar</button>
          </div>
        </div>

      </form>

    </div>

    <br><br>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script type="text/javascript">

      $("#foto").change(function() {

        if (this.files && this.files[0]) {
          var reader = new FileReader();

          reader.onload = function(e) {
            $('#visualizar-imagem').attr('src', e.target.result);
          }

          reader.readAsDataURL(this.files[0]);
        }

      });

    </script>
  </body>
</html>
