<?php

  include __DIR__ . "/../include.php";

  session_start();
  if(empty($_SESSION["usuario"])) {
    header("Location: ../login/?erro=3");
  }

  $sistemaDao = new SistemaDAO();

  if(!empty($_GET["del"])){

    $sistemaDao->excluir($_GET["del"]);
    header("Location: ?sucesso=1");

  }

?>

<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Painel de administrador - Sistemas - IFPI Campus Parnaíba</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#349650">

    <meta charset="utf-8">

  	<!-- FontAwesome CSS -->
  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="../style.css">

  </head>
  <body>

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark" style="background-color: #349650">
      <a class="navbar-brand" href="#">PAINEL ADMINISTRATIVO</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Sistemas <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Novo
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="./novosistema.php">Sistema</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">Usuário</a>
              <a class="dropdown-item" href="#">Senha</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="./logout.php">Sair</a>
          </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
          <input class="form-control mr-sm-2" type="search" name="busca" placeholder="Buscar sistema"  <?php if(!empty($_GET["busca"])) echo "value=\"" . $_GET["busca"] . "\""; ?>>
          <button class="btn btn-outline-light" type="submit"><i class="fas fa-search"></i></button>
        </form>
      </div>
    </nav>

    <br><br><br>

    <?php if (!empty($_GET["sucesso"])): ?>

      <div class="container">
        <div class="alert alert-success alert-dismissible fade show" role="alert">
          <?php

            switch($_GET["sucesso"]){
              case 1:
                echo "Sistema excluído com sucesso!";
                break;
              case 2:
                echo "Sistema cadastrado com sucesso!";
                break;
            }

          ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div>

    <?php endif; ?>

  	<?php if(!empty($_GET["busca"])): ?>

  		<div class="alert alert-light alerta-busca">
  			<?php echo $sistemaDao->quantidadeSistemasBusca($_GET["busca"]); ?> resultado(s) encontrados.
  			<a href="javascript:window.history.back(-1)" class="btn btn-link" >Voltar</a>
  		</div>

      <div class="container">
        <div class="row">

          <?php foreach ($sistemaDao->buscar($_GET["busca"]) as $sistema): ?>

            <div class="col-sm">

              <div class="sistema border rounded">

                <div class="sistema-content">
                  <img class="imagem-sistema border rounded" src="../img/sistemas/<?php echo $sistema['imagem']; ?>">
                  <div class="texto-sistema">
                    <h1 class="nome-sistema"><?php echo $sistema["nome"]; ?></h1>
                    <h2 class="descricao-sistema"><?php echo $sistema["descricao"]; ?></h2>
                  </div>
                </div>

                <div class="botoes">
                  <a href="<?php echo $sistema['endereco']; ?>" class="btn btn-success botao-sistema" target="_blank">Acessar</a>
                  <a href="javascript:apagar(<?php echo $sistema['id']; ?>)" class="btn btn-danger"><i class="fas fa-trash botao-excluir"></i></a>
                  <a href="" class="btn btn-primary"><i class="fas fa-edit botao-excluir"></i></a>
                </div>

              </div>

            </div>

          <?php endforeach; ?>

        </div>
      </div>


  	<?php endif; ?>

  	<div class="container">
  	  <div class="row">

  	  	<?php if(empty($_GET["busca"])): ?>
  	  		<?php foreach ($sistemaDao->consultarTodos() as $sistema): ?>

  			    <div class="col-sm">

  			    	<div class="sistema border rounded">

  							<div class="sistema-content">
  								<img class="imagem-sistema border rounded" src="../img/sistemas/<?php echo $sistema['imagem']; ?>">
  								<div class="texto-sistema">
  									<h1 class="nome-sistema"><?php echo $sistema["nome"]; ?></h1>
  									<h2 class="descricao-sistema"><?php echo $sistema["descricao"]; ?></h2>
  								</div>
  							</div>

                <div class="botoes">
                  <a href="<?php echo $sistema['endereco']; ?>" class="btn btn-success botao-sistema" target="_blank">Acessar</a>
                  <a href="javascript:apagar(<?php echo $sistema['id']; ?>)" class="btn btn-danger"><i class="fas fa-trash botao-excluir"></i></a>
                  <a href="" class="btn btn-primary"><i class="fas fa-edit botao-excluir"></i></a>
                </div>

  			    	</div>

  			    </div>

  	    	<?php endforeach; ?>
  		<?php endif; ?>

  	  </div>
  	</div>

    <script type="text/javascript">

      function apagar(id){
        if(confirm("Tem certeza que deseja apagar este sistema?")){
          window.location.href = "?del=" + id;
        }
      }

    </script>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

  </body>
</html>
