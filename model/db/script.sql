CREATE DATABASE IF NOT EXISTS sistemas_capar
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;

USE sistemas_capar;

CREATE TABLE sistemas(
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(100),
  descricao VARCHAR(500),
  endereco VARCHAR(100),
  imagem VARCHAR(50),

  PRIMARY KEY(id)
);

CREATE TABLE admin(
  id INT NOT NULL AUTO_INCREMENT,
  nome VARCHAR(100),
  user VARCHAR(50),
  senha VARCHAR(60),

  PRIMARY KEY(id)
);

INSERT INTO sistemas VALUES
(DEFAULT, "SisEgressos", "Sistema para coletar e verificar dados de alunos egressos do IFPI Campus Parnaíba.", "http://187.125.105.77/egressos", "default.jpg");

INSERT INTO admin VALUES
(DEFAULT, "Rei da Cocada", "admin", "$2b$10$EMExuOtRkt5OZbwaFkehXe54L2jCHW/HF4dFEUoXmcY9mWmTNJ9uu");