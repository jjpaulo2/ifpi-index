<?php

	include __DIR__ . "/../include.php";

	session_start();
	if(!empty($_SESSION["usuario"])) {
		header("Location: ../admin");
	}

	$dao = new AdminDAO();

	if(!empty($_POST["usuario"])) {

		if($dao->existeUsuario($_POST["usuario"])) {

			if($dao->login($_POST["usuario"], $_POST["senha"])) {

				$_SESSION["usuario"] = $_POST["usuario"];
				header("Location: ../admin");

			}
			else {
				header("Location: ?erro=2");
			}

		}
		else {
			header("Location: ?erro=1");
		}

	}

?>

<!DOCTYPE html>
<html lang="pt-br" dir="ltr">
  <head>
    <meta charset="utf-8">

    <title>Área restrita - Sistemas - IFPI Campus Parnaíba</title>


  	<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="theme-color" content="#349650">

  	<!-- FontAwesome CSS -->
  	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

  	<!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="../style.css">
		<link rel="stylesheet" type="text/css" href="./login.css">

  </head>
  <body>

    <div class="box-login">

			<?php if (!empty($_GET["erro"])): ?>

				<div class="alert alert-danger alert-dismissible fade show" role="alert">

					<?php

						switch ($_GET['erro']) {
							case 1:
								echo "O usuário inserido não conta em nossa base de dados.";
								break;
							case 2:
								echo "Senha incorreta! Por favor, tente novamente.";
								break;
							case 3:
								echo "Faça login primeiro!";
								break;
						}

					?>

				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>

			<?php endif; ?>

      <a class="btn btn-link" href="..">Retornar ao início</a>
      <form class="login rounded border" method="post">

        <div class="form-group">

          <label for="usuario">Usuário</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="user-icon"><i class="fas fa-user"></i></span>
            </div>
            <input type="text" class="form-control" name="usuario" id="usuario" placeholder="Ex.: jjpaulo2" required>
          </div>

        </div>
        <div class="form-group">

          <label for="senha">Senha</label>
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="user-icon"><i class="fas fa-lock"></i></span>
            </div>
            <input type="password" class="form-control" name="senha" id="senha" placeholder="********" required>
          </div>

        </div>

        <button type="submit" class="btn btn-success" style="margin: 0 0 auto auto; width: 100px; display:block;">Entrar</button>

      </form>

    </div>


		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


  </body>
</html>
